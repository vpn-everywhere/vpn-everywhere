Mémo pour les commandes GIT


Pour récupérer le dépôt :

Depuis le cremi 	=> git clone https://services.emi.u-bordeaux.fr/projet/git/pdpvpn



Pour récupérer les dernières modifications :

git pull


Pour ajouter vos modifications ou des nouveaux fichiers / répertoires :

git add [vos fichiers / répertoires modifiés]


Pour sauvegarder "localement" vos modifications :

git commit -m "[Votre message]"

Pour mettre à jour le dépôt <=> partager vos dernières modifications

Première fois 			=> git push origin master
Si déjà fait une fois 	=> git push


Pour voir l'état de votre copie locale de travail

git status

