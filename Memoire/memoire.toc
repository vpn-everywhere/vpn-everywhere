\select@language {french}
\contentsline {section}{\numberline {1}\IeC {\'E}tude de l'existant}{4}
\contentsline {section}{\numberline {2}Analyse des besoins}{6}
\contentsline {subsection}{\numberline {2.1}Diagramme}{6}
\contentsline {subsection}{\numberline {2.2}Besoins fonctionnels}{7}
\contentsline {paragraph}{ Le scan des ports: }{7}
\contentsline {paragraph}{G\IeC {\'e}n\IeC {\'e}ration du fichier de configuration: }{8}
\contentsline {paragraph}{Lancement avec le serveur: }{9}
\contentsline {paragraph}{Test: }{9}
\contentsline {subsection}{\numberline {2.3}GUI et Graphe de Fonctionnement de l'interface}{9}
\contentsline {subsection}{\numberline {2.4}Besoins non fonctionnels}{9}
\contentsline {subsubsection}{\numberline {2.4.1}Besoins comportementaux}{9}
\contentsline {paragraph}{2.4.1.1 Vitesse: }{9}
\contentsline {paragraph}{2.4.1.2 Facilit\IeC {\'e} d'utilisation: }{10}
\contentsline {paragraph}{2.4.1.3 Robustesse: }{10}
\contentsline {paragraph}{2.4.1.4 Les difficult\IeC {\'e}s techniques: }{10}
\contentsline {subsubsection}{\numberline {2.4.2}Besoins organisationnels}{10}
\contentsline {subsubsection}{\numberline {2.4.3}Besoins externes}{11}
\contentsline {paragraph}{2.4.3.1 Contraintes de Portabilit\IeC {\'e}: }{11}
\contentsline {paragraph}{2.4.3.2 Contraintes l\IeC {\'e}gales:}{11}
\contentsline {subsubsection}{\numberline {2.4.4}Gestion du temps}{11}
\contentsline {subsection}{\numberline {2.5}Diagramme de s\IeC {\'e}quence }{12}
\contentsline {subsection}{\numberline {2.6}Type d'erreurs}{12}
\contentsline {section}{\numberline {3}Architecture}{13}
\contentsline {subsection}{\numberline {3.1}Vue d\IeC {\textquoteright }ensemble}{13}
\contentsline {subsection}{\numberline {3.2}Architecture de l'application (VPN EVERYWHER)}{13}
\contentsline {subsection}{\numberline {3.3}Diagrammes de classe}{14}
\contentsline {section}{\numberline {4}Impl\IeC {\'e}mentation}{16}
\contentsline {subsection}{\numberline {4.1}Heuristique}{16}
\contentsline {subsection}{\numberline {4.2}Biblioth\IeC {\`e}que Scan des ports (UDP/TCP)}{16}
\contentsline {subsection}{\numberline {4.3}Le scan des ports TCP/UDP de n\IeC {\textquoteright }importe quel serveur VPN (cas g\IeC {\'e}n\IeC {\'e}ral):}{17}
\contentsline {subsubsection}{\numberline {4.3.1}Scan TCP(TcpScan ())}{17}
\contentsline {subsubsection}{\numberline {4.3.2}Scan UDP(PortScanUdp())}{17}
\contentsline {subsection}{\numberline {4.4}Am\IeC {\'e}lioration du Scan des ports UDP/TCP}{18}
\contentsline {subsubsection}{\numberline {4.4.1}Scan TCP en utilisant aquilenet}{19}
\contentsline {subsubsection}{\numberline {4.4.2}Scan UDP en utilisant aquilenet}{19}
\contentsline {subsection}{\numberline {4.5}Gestion de scan des ports }{19}
\contentsline {subsubsection}{\numberline {4.5.1}PortScanManager ():}{20}
\contentsline {subsection}{\numberline {4.6}Parall\IeC {\'e}lisme des traitements}{20}
\contentsline {subsection}{\numberline {4.7}Interface}{21}
\contentsline {subsection}{\numberline {4.8}Fichier de configuration}{24}
\contentsline {subsection}{\numberline {4.9}Lancement de serveur}{24}
\contentsline {section}{\numberline {5}Tests}{25}
\contentsline {subsection}{\numberline {5.1}Tests unitaires}{25}
\contentsline {subsection}{\numberline {5.2}Tests de fonctionnement de la biblioth\IeC {\`e}que Scan}{26}
\contentsline {subsection}{\numberline {5.3}Tests de fichier configuration}{27}
\contentsline {subsection}{\numberline {5.4}Tests de fonctionnement}{27}
\contentsline {subsubsection}{\numberline {5.4.1}Tests du bon fonctionnement de la biblioth\IeC {\`e}que de scan}{27}
\contentsline {section}{\numberline {6}Conclusion}{29}
\contentsline {section}{\numberline {7}Bibliographique}{30}
